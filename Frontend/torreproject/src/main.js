import Vue from 'vue';
import App from './App.vue';
import router from './router';
import vuetify from '@/plugins/vuetify'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import {truncate} from './assets/js/utils'


Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.filter('truncate', truncate);

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
