export const URL_SERVER_USER_INFO = "http://localhost:8000/api/user/"
export const URL_SERVER_OPORTUNITIES = "http://localhost:8000/api/oportunities/"
export const URL_OPPORTUNITIES = "http://localhost:8000/api/search_job/"
export const URL_PEOPLE = "http://localhost:8000/api/search_people/"
export const FILTERS_JOBS = [
                                { title: 'For you', options: ["Best For You", "Your dream Jobs"] },
                            ]
