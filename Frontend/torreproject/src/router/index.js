import Vue from 'vue'
import VueRouter from 'vue-router'
import Profile from '../views/Profile'
import Oportunities from '../views/Oportunities'
import SearchPeople from '../views/SearchPeople'
import SearchJob from '../views/SearchJobs'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'SearchJob',
    component: SearchJob
  },
  {
    path: '/profile/:username',
    name: 'Profile',
    component: Profile
  },
  {
    path: '/jobs/:id',
    name: 'Oportunities',
    component: Oportunities
  },
  {
    path: '/search/people/',
    name: 'SearchPeople',
    component: SearchPeople
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
