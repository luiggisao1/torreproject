""" Endpoints """

from django.urls import path
from . import views


urlpatterns = [
    path(r'user/<str:username>', views.user_info, name="user-info"),
    path(r'oportunities/<str:id_job>', views.oportunity_info, name='oportunity-info'),
    path(r'search_job/', views.search_job, name='search-job'),
    path(r'search_people/', views.search_people, name='search-people')
]
