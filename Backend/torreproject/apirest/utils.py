""" Utilities """
from datetime import datetime
import calendar
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize
import nltk
nltk.download("stopwords")
nltk.download("punkt")


URL_TORRE_BIO = 'https://torre.bio/api/bios/'
URL_TORRE_OPORTUNITY = 'https://torre.co/api/opportunities/'
URL_TORRE_SEARCH_JOB  = 'https://search.torre.co/opportunities/_search/'
URL_TORRE_SEARCH_PEOPLE = 'https://search.torre.co/people/_search/'
DEFAULT_EXPERIENCE_FILTER = 'potential-to-develop'

def clean_text(text):

	stop_words = set(stopwords.words('english')) 
	word_tokens = word_tokenize(text)
	word_tokens = word_tokenize(text) 
  
	filtered_sentence = [w for w in word_tokens if not w in stop_words] 
	  
	return " ".join(filtered_sentence)
  

def parse_search_parameters(url, offset, size, aggregators):
    """ A simple function that returns the URL in order to make post to Torre """

    offset = offset if offset else 0
    size = size if size else 10
    aggregators = aggregators if aggregators else ""
    parameters = "?offset=" + str(offset) + "&size=" + str(size) + "&aggregate=" + aggregators

    return url + parameters

def parse_body_filters(filter_type, text):
	body = {}
	text = clean_text(text)
	if filter_type == 'skill/role':
		dic = {"text": text, "experience": DEFAULT_EXPERIENCE_FILTER}
	elif filter_type == 'organization':
		dic = {"term": text}
	body[filter_type] = dic

	return body

def parse_time(time):
	if time:
		date_datetime = datetime.strptime(time, "%Y-%m-%dT%H:%M:%S.%fZ")
		starttime = date_datetime.strftime("%m %d, %Y %H:%M:%S")
		return starttime

def parse_members(members):
	response_members = []
	for member in members:
		dic = {}
		dic["subjectId"] = member["subjectId"]
		dic["name"] = member['name']
		dic['picture'] = member['picture']
		dic['username'] = member['username']
		response_members.append(dic)
	return response_members

def parse_organizations(organizations, members):
	if len(organizations) > 0:
		return organizations[0]
	else:
		organizations = {}
		organizations["name"] = members[0]["name"]
		organizations["picture"] = None
		return organizations

def parse_skills(skills):
	skills_parsed = []
	n_skills = len(skills)
	if n_skills > 7:
		skills = skills[0:6]
	for skill in skills:
		dic = {}
		experience = skill["experience"]
		digit = experience.split("-")[0]
		if digit.isdigit():
			experience = digit + "+" + "years"
		else:
			experience = ""
		dic["name"] = skill["name"]
		dic['experience'] = experience

		skills_parsed.append(dic)

	return skills_parsed

def parse_members(members):
	n_members = len(members)
	if n_members > 3:
		return members[0:2]
	else:
		return members



def parse_response(response):
	opportunities = response.json()
	results = opportunities['results']
	datas = []
	for result in results:
		data = {}
		data['id'] = result["id"]
		data["title"] = result["objective"]
		data["type"] = result['type']
		data['starttime'] = parse_time(result['created'])
		data['endtime'] = parse_time(result['deadline'])
		data["members"] = parse_members(result["members"])
		organizations = parse_organizations(result["organizations"], data["members"])
		data['name'] = organizations["name"]
		data['logo'] = organizations["picture"]
		data["compensations"] = result["compensation"]
		data['locations'] = result['locations']
		data['skills'] = parse_skills(result['skills'])
		data['remote'] = result['remote']
		data['parent'] = "Oportunities"

		datas.append(data)

	return datas

def parse_time_opportunity(time):
	if time:
		date_datetime = datetime.strptime(time, "%Y-%m-%dT%H:%M:%SZ")
		starttime = date_datetime.strftime("%m %d, %Y %H:%M:%S")
		return starttime

def parse_baseSalary(baseSalary):
	currency = baseSalary['currency']
	minValue = baseSalary['minAmount']
	maxValue = baseSalary['maxAmount']
	unitText = baseSalary['periodicity']
	if maxValue:
		result = currency + str(minValue) + " - " + str(maxValue) + "/ " + unitText.lower()
	else:
		result = currency + str(minValue) + "/ " + unitText.lower()
	return result

def get_reason_job(details):
	for detail in details:
		if detail['code'] == "reason":
			return detail['content']

def parsed_responsibilities(details):
	responsibilities = []
	for detail in details:
		if detail['code'] == "responsibilities":
			contents = detail['content']
			contents = contents.split("\n")
			for content in contents:
				responsibilities.append(content)

	return responsibilities

def parsed_opportunity(response):
	response_parsed = {}
	data = response.json()
	response_parsed['picture'] = data['attachments'][0]['address']
	response_parsed['objective'] = data['objective']
	response_parsed['strengths'] = parse_skills(data['strengths'])
	response_parsed['starttime'] = parse_time_opportunity(data['created'])
	response_parsed['endtime'] = parse_time_opportunity(data['deadline'])
	response_parsed['hiringOrganization'] = data['organizations'][0]
	response_parsed['baseSalary'] = parse_baseSalary(data['compensation'])
	response_parsed['reason'] = get_reason_job(data['details'])
	response_parsed['responsibilities'] = parsed_responsibilities(data['details'])
	return response_parsed

def parse_skills_people(skills):
	skills_parsed = []
	n_skills = len(skills)
	if n_skills > 5:
		skills = skills[0:6]
	for skill in skills:
		dic = {}
		dic["name"] = skill["name"]


		skills_parsed.append(dic)

	return skills_parsed


def parse_people_response(response):
	data = response.json()
	results = data['results']
	people = []
	for result in results:

		response_parsed = {}
		response_parsed['id'] = result['username']
		response_parsed['logo'] = result['picture']
		response_parsed['title'] = result['name']
		print(result['name'])
		response_parsed['name'] = result['professionalHeadline']
		response_parsed['locations'] = [result['locationName']]
		response_parsed['skills'] = parse_skills_people(result['skills'])
		response_parsed['openTo'] = result['openTo']
		people.append(response_parsed)
	return people

def verify_picture(person):
	keys = person.keys()
	print(keys)
	if "picture" in keys:
		return person['picture']


def parse_person(response):
	data = response.json()
	person = data['person']
	response_parsed = {}
	response_parsed['name'] = person['name']
	response_parsed['picture'] = verify_picture(person)
	response_parsed['strengths'] = data['strengths']
	response_parsed['languages'] = data['languages']
	response_parsed['professionalHeadline'] = person['professionalHeadline']
	response_parsed['interests'] = data['interests']
	return response_parsed

def parse_body_filters_people(filter, text):
	dic = {}
	dic['name'] = {"term":text}

	return dic