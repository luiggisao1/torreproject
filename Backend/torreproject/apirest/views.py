""" View for each URL """
import json
from rest_framework import status
from rest_framework.decorators import api_view
from django.http import HttpResponse
import requests
from .utils import URL_TORRE_BIO, URL_TORRE_OPORTUNITY, URL_TORRE_SEARCH_JOB, parse_people_response, parse_person, parse_body_filters_people
from .utils import URL_TORRE_SEARCH_PEOPLE, parse_search_parameters, parse_body_filters, parse_response, parsed_opportunity



@api_view(['GET'])
def user_info(request, username):
    """ Get User info from Torre """

    url = URL_TORRE_BIO + username
    response = requests.get(url)
    if response.status_code == 200:
        reponse_parsed = parse_person(response)
        return HttpResponse(json.dumps(reponse_parsed),
                            content_type='application/json')
    return HttpResponse(json.dumps({}), content_type='application/json',
                        status=status.HTTP_404_NOT_FOUND)



@api_view(['GET'])
def oportunity_info(request, id_job):
    """ Get Job info from Torre """

    url = URL_TORRE_OPORTUNITY + id_job
    response = requests.get(url)
    if response.status_code == 200:
        response_parsed = parsed_opportunity(response)
        return HttpResponse(json.dumps(response_parsed),
                            content_type='application/json')
    return HttpResponse(json.dumps({}), content_type='application/json',
                        status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
def search_job(request):
    """ Post Job to Torre """

    offset = request.data.get('offset')
    size = request.data.get('size')
    aggregators = request.data.get('aggregators')
    text = request.data.get("text")
    filter_type = request.data.get('filter_type')
    if offset or size or aggregators:
        url = parse_search_parameters(URL_TORRE_SEARCH_JOB, offset, size, aggregators)
    else:
        url = URL_TORRE_SEARCH_JOB
    payload = parse_body_filters(filter_type, text)
    response = requests.post(url, json=payload)
    response_parsed = parse_response(response)
    return HttpResponse(json.dumps(response_parsed), content_type='application/json')


@api_view(['POST'])
def search_people(request):
    """ Post People to Torre """

    offset = request.data.get('offset')
    size = request.data.get('size')
    aggregators = request.data.get('aggregators')
    text = request.data.get("text")
    filter_type = request.data.get('filter_type')
    if offset or size or aggregators:
        url = parse_search_parameters(URL_TORRE_SEARCH_PEOPLE, offset, size, aggregators)
    else:
        url = URL_TORRE_SEARCH_PEOPLE
    payload = parse_body_filters_people(filter_type, text)
    print(payload)
    response = requests.post(url, json=payload)
    response_parsed = parse_people_response(response)
    return HttpResponse(json.dumps(response_parsed), content_type='application/json')
