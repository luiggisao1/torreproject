""" Module to Test APIs Endpoints """

from rest_framework import status
from django.test import Client
from django.urls import reverse
import pytest


client = Client()


@pytest.mark.django_db
def test_get_user_info():
    """ Successful Test to retrieve data of a specific user with their username """

    response = client.get(
        reverse('user-info', kwargs={'username': 'luiggialg97'})
    )

    expected_response = {
        'status': status.HTTP_200_OK,
    }

    assert response.status_code == expected_response['status']


@pytest.mark.django_db
def test_failed_get_user_info():
    """ Failed Test to retrieve data of a specific user with their username """

    response = client.get(
        reverse('user-info', kwargs={'username': 'unknown'})
    )

    expected_response = {
        'status': status.HTTP_404_NOT_FOUND,
    }

    assert response.status_code == expected_response['status']

@pytest.mark.django_db
def test_get_oportunity_info():
    """ Successful Test to retrieve data of a specific job with their id """

    response = client.get(
        reverse('oportunity-info', kwargs={'id': 'OwGBNOW6'})
    )

    expected_response = {
        'status': status.HTTP_200_OK,
    }

    assert response.status_code == expected_response['status']

@pytest.mark.django_db
def test_failed_get_oportunity_info():
    """ Failed Test to retrieve data of a specific job with their id """

    response = client.get(
        reverse('oportunity-info', kwargs={'id': 'unknown'})
    )

    expected_response = {
        'status': status.HTTP_404_NOT_FOUND,
    }

    assert response.status_code == expected_response['status']
