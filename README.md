# torreproject


## Updates
Algolia search is not being used anymore.

Instead I transform every request in a body request for the Torre endpoints.

NLTK (Python library) is used as a Text analytic, using stopwords I can extract the text from the search. For example: If someone is looking for a job like "developer in Ecuador", the word "in" is not used.

Filter jobs by skill/role and organization are supported, the others are not supported.

The files for the Frontend framework are separated for for better scalability and maintainability
### Design
The color palette is the same as the main Torre page

New cards design.

Observer design pattern is used in the project. For example if you choose a filter for searching jobs it will be different results. For example organizations.

Profile for people and jobs have a new design.


## How it works
The app is deployed in Heroku.
https://torreproject.herokuapp.com/ 

In the Homepage you can search for a job. The results will be any job that matches the query. Try searching "data engineer".

The people tab is for searching people and works exact as the job search.

If you do not find a specific search is because I used Algolia for searching filters and I had to upload the data to the platform and already has 2200 records for people and jobs.

After searching you can see the info about that person or job by clicking the picture. That will redirect you to https://torreproject.herokuapp.com/profile/:username or https://torreproject.herokuapp.com/jobs/:id 

### Design
I know The design is not beautiful but I tried my best in a shor time. I was focus on the functionality of the app in this case. I was designing a template in AdobeXD but there was not time

### Technologies
For Backend I used Django framework. For Frontend I used Vue.js framework.
For searches I use Algolia. This is a very useful tool for data searches on websites.

### Issues
-The first issue I faced was the CORS policy when trying to make a request to the Tower API from a local client in Vue.js. The problem was solved by sending a request to a local server developed in Django, then I sent the request to the Tower API and there were no problems because the same response was sent to the Vue client.

-Another big Issue was that I had no any ideas for the design. I tried my best in a short time.
